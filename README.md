# SAM Resource Project

This project defines AWS resources using AWS Serverless Application Model (SAM). The resources include a DynamoDB table, SNS topic, and SQS queues, along with necessary policies and subscriptions.

## Resources

The following resources are defined in `template.yaml`:

- **DynamoDB Table**: Stores running numbers.
- **SNS Topic**: Manages lead events.
- **SQS Queues**: 
  - `lead-create` queue
  - `lead-sync` queue
  - Dead-letter queues (DLQs) for both lead-create and lead-sync queues.

## Prerequisites

- AWS CLI installed and configured
- AWS SAM CLI installed
- An AWS account with necessary permissions

## Setup

1. **Clone the repository:**

    ```sh
    git clone <repository-url>
    cd <repository-directory>
    ```

2. **Install dependencies (if any):**

    ```sh
    # Example for Node.js
    npm install
    ```

## Deployment

To deploy the SAM application, follow these steps:

1. **Build the SAM application:**

    ```sh
    sam build
    ```

2. **Package the SAM application:**

    ```sh
    sam package --output-template-file packaged.yaml --s3-bucket <your-s3-bucket>
    ```

3. **Deploy the SAM application:**

    ```sh
    sam deploy --template-file packaged.yaml --stack-name <your-stack-name> --capabilities CAPABILITY_IAM
    ```

## Resources

The following resources are created by this SAM application:

- **DynamoDB Table**: `${stage}-lead-running-number`
- **SNS Topic**: `${stage}-lead-management.fifo`
- **SQS Queues**:
  - Lead Create Queue: `${stage}-lead-create.fifo`
  - Lead Sync Queue: `${stage}-lead-sync.fifo`
  - Dead-letter Queue for Lead Create: `${stage}-dlq-lead-create.fifo`
  - Dead-letter Queue for Lead Sync: `${stage}-dlq-lead-sync.fifo`

## Policies and Subscriptions

- **Queue Policies**: Allow SNS topic to send messages to SQS queues.
- **SNS Subscriptions**: SQS queues subscribed to the SNS topic with filter policies.

## Example Usage

To test the deployment, you can publish a message to the SNS topic and verify that it is received by the appropriate SQS queue based on the subject filter.

```sh
aws sns publish \
    --topic-arn <SNS-topic-ARN> \
    --message '{"key": "value"}' \
    --message-attributes '{"subject":{"DataType":"String","StringValue":"LEAD_CREATE"}}' \
    --message-deduplication-id <unique-id> \
    --message-group-id <group-id>
```